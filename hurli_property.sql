--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: seq_hp_pages_id; Type: SEQUENCE; Schema: public; Owner: hurl
--

CREATE SEQUENCE seq_hp_pages_id
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_hp_pages_id OWNER TO hurl;

--
-- Name: seq_hp_pages_id; Type: SEQUENCE SET; Schema: public; Owner: hurl
--

SELECT pg_catalog.setval('seq_hp_pages_id', 1, false);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: hp_pages; Type: TABLE; Schema: public; Owner: hurl; Tablespace: 
--

CREATE TABLE hp_pages (
    id integer DEFAULT nextval('seq_hp_pages_id'::regclass) NOT NULL,
    url character varying(256) NOT NULL,
    password character varying(128)
);


ALTER TABLE public.hp_pages OWNER TO hurl;

--
-- Name: hp_properties; Type: TABLE; Schema: public; Owner: hurl; Tablespace: 
--

CREATE TABLE hp_properties (
    id integer NOT NULL,
    title character varying NOT NULL,
    description character varying,
    link character varying,
    image character varying DEFAULT 'default.png'::character varying,
    created timestamp without time zone NOT NULL,
    pub_date timestamp without time zone,
    price character varying DEFAULT '0'::character varying
);


ALTER TABLE public.hp_properties OWNER TO hurl;

--
-- Name: hp_properties_id_seq; Type: SEQUENCE; Schema: public; Owner: hurl
--

CREATE SEQUENCE hp_properties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.hp_properties_id_seq OWNER TO hurl;

--
-- Name: hp_properties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hurl
--

ALTER SEQUENCE hp_properties_id_seq OWNED BY hp_properties.id;


--
-- Name: hp_properties_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hurl
--

SELECT pg_catalog.setval('hp_properties_id_seq', 383, true);


--
-- Name: hp_properties_queries; Type: TABLE; Schema: public; Owner: hurl; Tablespace: 
--

CREATE TABLE hp_properties_queries (
    property_id integer NOT NULL,
    query_id integer NOT NULL
);


ALTER TABLE public.hp_properties_queries OWNER TO hurl;

--
-- Name: hp_queries; Type: TABLE; Schema: public; Owner: hurl; Tablespace: 
--

CREATE TABLE hp_queries (
    id integer NOT NULL,
    query character varying NOT NULL,
    created timestamp without time zone NOT NULL
);


ALTER TABLE public.hp_queries OWNER TO hurl;

--
-- Name: hp_queries_id_seq; Type: SEQUENCE; Schema: public; Owner: hurl
--

CREATE SEQUENCE hp_queries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.hp_queries_id_seq OWNER TO hurl;

--
-- Name: hp_queries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hurl
--

ALTER SEQUENCE hp_queries_id_seq OWNED BY hp_queries.id;


--
-- Name: hp_queries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hurl
--

SELECT pg_catalog.setval('hp_queries_id_seq', 22, true);


--
-- Name: seq_hp_users_id; Type: SEQUENCE; Schema: public; Owner: hurl
--

CREATE SEQUENCE seq_hp_users_id
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seq_hp_users_id OWNER TO hurl;

--
-- Name: seq_hp_users_id; Type: SEQUENCE SET; Schema: public; Owner: hurl
--

SELECT pg_catalog.setval('seq_hp_users_id', 1, true);


--
-- Name: hp_users; Type: TABLE; Schema: public; Owner: hurl; Tablespace: 
--

CREATE TABLE hp_users (
    id integer DEFAULT nextval('seq_hp_users_id'::regclass) NOT NULL,
    username character varying(64) NOT NULL,
    email character varying(255) NOT NULL,
    first_name character varying(64) NOT NULL,
    last_name character varying(64) NOT NULL,
    password character varying(128) NOT NULL
);


ALTER TABLE public.hp_users OWNER TO hurl;

--
-- Name: hp_users_pages; Type: TABLE; Schema: public; Owner: hurl; Tablespace: 
--

CREATE TABLE hp_users_pages (
    user_id integer NOT NULL,
    page_id integer NOT NULL
);


ALTER TABLE public.hp_users_pages OWNER TO hurl;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: hurl
--

ALTER TABLE hp_properties ALTER COLUMN id SET DEFAULT nextval('hp_properties_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: hurl
--

ALTER TABLE hp_queries ALTER COLUMN id SET DEFAULT nextval('hp_queries_id_seq'::regclass);


--
-- Data for Name: hp_pages; Type: TABLE DATA; Schema: public; Owner: hurl
--



--
-- Data for Name: hp_properties; Type: TABLE DATA; Schema: public; Owner: hurl
--

INSERT INTO hp_properties VALUES (344, 'Oasis On Woods', 'Construction Has Commenced Oasis On Woods; Exclusive Apartments For People With Higher Expectations A Full Detailed Oasis Pack Is...', NULL, 'http://images1.au.reastatic.net/126x94/props/106913452/Main@20Feature/20101015135700.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '805,000');
INSERT INTO hp_properties VALUES (345, 'Oasis On Woods', 'Construction Has Commenced Oasis On Woods; Exclusive Apartments For People With Higher Expectations A Full Detailed Oasis Pack Is...', NULL, 'http://images1.au.reastatic.net/126x94/props/106913448/Main@20Feature/20101015135700.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '865,000');
INSERT INTO hp_properties VALUES (346, 'Oasis On Woods', 'Construction Has Commenced Oasis On Woods; Exclusive Apartments For People With Higher Expectations A Full Detailed Oasis Pack Is...', NULL, 'http://images2.au.reastatic.net/126x94/props/106913442/Main@20Feature/20101015135700.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '890,000');
INSERT INTO hp_properties VALUES (347, 'Oasis On Woods', 'Construction Has Commenced Oasis On Woods; Exclusive Apartments For People With Higher Expectations A Full Detailed Oasis Pack Is...', NULL, 'http://images1.au.reastatic.net/126x94/props/106913414/Main@20Feature/20101015135600.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '790,000');
INSERT INTO hp_properties VALUES (348, 'Oasis On Woods', 'Construction Has Commenced Oasis On Woods; Exclusive Apartments For People With Higher Expectations A Full Detailed Oasis Pack Is...', NULL, 'http://images2.au.reastatic.net/126x94/props/106913410/Main@20Feature/20101015135600.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '750,000');
INSERT INTO hp_properties VALUES (349, 'One Not To Be Missed', 'This Stunning Apartment On Level 20 Of The Prestigious Evolution Complex Offers Everything You Have Been Dreaming Of In A...', NULL, 'http://images1.au.reastatic.net/126x94/props/106626826/Main@20Feature/20100824105858.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '12');
INSERT INTO hp_properties VALUES (350, 'Life Style To Its Highest Order-pure Elegance', 'Quite Simply; The 33 Storey, International Standard, Architecturally Designed Evolution On Gardiner Is Darwin''s Finest Address....', NULL, 'http://images1.au.reastatic.net/126x94/props/106453822/Main@20Feature/20090908143200.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '1,350,000');
INSERT INTO hp_properties VALUES (351, 'The Rest Is Up To Your Imagination!', 'Place Yourself In To A World Of Ease, Luxury And Timeless Elegance At Evolution On Gardiner, Darwin''s Landmark Residential Sky...', NULL, 'http://images2.au.reastatic.net/126x94/props/106421924/Main@20Feature/20101117130000.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '0');
INSERT INTO hp_properties VALUES (352, 'Many Options, Rural Lifestyle, Town Close.', 'Highly Desirable Location. A Large Family Home Set Upon 2 Outstanding Acres. · Main Residence 4 Bedrooms Plus Office · Huge Tiled...', NULL, 'http://images2.au.reastatic.net/126x94/props/106362524/Main@20Feature/20100312144410.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '1,620,000');
INSERT INTO hp_properties VALUES (353, 'Darwin City Location', 'Elevated Dwelling Situated Towards The Front Of The Block. The 3 Bedroom Home Is In Original Condition And Requires Updating....', NULL, 'http://images2.au.reastatic.net/126x94/props/106004340/Main@20Feature/20100508163927.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '0');
INSERT INTO hp_properties VALUES (354, 'Boutique Apartment Provides An Ideal City Investement', 'As A Point Of Difference Within The Market This Three Bedroom Apartment Defies The Odds. There Are Only 2 X 3 Bedroom Apartments...', NULL, 'http://images2.au.reastatic.net/126x94/props/105526697/Main@20Feature/20100305005859.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '640,000');
INSERT INTO hp_properties VALUES (355, 'Dha Investment Apartment', 'Offering Stunning Ocean Views From The Large Balcony This Property Is Packed With Features And Is A Solid Investment. 3 Minutes...', NULL, 'http://images1.au.reastatic.net/150x112/props/107152296/Main@20Feature/20110202142900.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '465,000');
INSERT INTO hp_properties VALUES (356, 'Capitilise By Buying At Today''s Prices!', 'Brought To You By Cento Pty Ltd And Kim Enterprises Is This Exciting Opportunity To Purchase Premium City Apartments At...', NULL, 'http://images1.au.reastatic.net/126x94/props/106976924/Main@20Feature/20101105123500.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '485,000');
INSERT INTO hp_properties VALUES (357, 'Capitilise By Buying At Today''s Prices!', 'Brought To You By Cento Pty Ltd And Kim Enterprises Is This Exciting Opportunity To Purchase Premium City Apartments At...', NULL, 'http://images2.au.reastatic.net/126x94/props/106969949/Main@20Feature/20101105123700.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '565,000');
INSERT INTO hp_properties VALUES (358, 'Capitilise By Buying At Today''s Prices!', 'Brought To You By Cento Pty Ltd And Kim Enterprises Is This Exciting Opportunity To Purchase Premium City Apartments At...', NULL, 'http://images1.au.reastatic.net/126x94/props/106969943/Main@20Feature/20101105123300.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '570,000');
INSERT INTO hp_properties VALUES (359, 'Capitilise By Buying At Today''s Prices!', 'Brought To You By Cento Pty Ltd And Kim Enterprises Is This Exciting Opportunity To Purchase Premium City Apartments At...', NULL, 'http://images2.au.reastatic.net/126x94/props/106969785/Main@20Feature/20101105124500.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '595,000');
INSERT INTO hp_properties VALUES (360, 'Capitilise By Buying At Today''s Prices!', 'Brought To You By Cento Pty Ltd And Kim Enterprises Is This Exciting Opportunity To Purchase Premium City Apartments At...', NULL, 'http://images1.au.reastatic.net/126x94/props/106969776/Main@20Feature/20101105124200.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '540,000');
INSERT INTO hp_properties VALUES (361, 'Capitilise By Buying At Today''s Prices!', 'Brought To You By Cento Pty Ltd And Kim Enterprises Is This Exciting Opportunity To Purchase Premium City Apartments At...', NULL, 'http://images2.au.reastatic.net/126x94/props/106952373/Main@20Feature/20101102075900.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '515,000');
INSERT INTO hp_properties VALUES (362, 'Capitilise By Buying At Today''s Prices!', 'Brought To You By Cento Pty Ltd And Kim Enterprises Is This Exciting Opportunity To Purchase Premium City Apartments At...', NULL, 'http://images1.au.reastatic.net/126x94/props/106952367/Main@20Feature/20101102075700.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '610,000');
INSERT INTO hp_properties VALUES (363, 'Capitilise By Buying At Today''s Prices!', 'Brought To You By Cento Pty Ltd And Kim Enterprises Is This Exciting Opportunity To Purchase Premium City Apartments At...', NULL, 'http://images2.au.reastatic.net/126x94/props/106952365/Main@20Feature/20101102080100.jpg', '2011-02-10 17:00:32', '2011-02-10 17:00:32', '505,000');
INSERT INTO hp_properties VALUES (364, '$120,000 - 102-104 Queen Street Barraba', 'Time For A Tree Change? Price Slashed To Sell!3 Bedroom Home With Large Enclosed Yard And An Operating Take Away/cafe To Boot! What More Could You Wish For ? For A Little More Than The Price Of An Average House You Can Earn An Income F...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008421896', 'http://images.domain.com.au/img/2010611/16618/f8d8a36e-439e-4e64-992e-26cc6b02d509_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 06:12:47', '120,000');
INSERT INTO hp_properties VALUES (365, '$280,000 - 87 Dalton Street Dubbo', 'Colonial Home In Southenjoying One Of The Best Locations In South Dubbo, This Three Bedroom Colonial Home With Front Verandah Offers The Best In Family Comforts. Well Maintained & Within Walking Distance Of Primary & High Schools, Boundary...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820257', 'http://images.domain.com.au/img/2011211/13906/2008820257_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 03:10:14', '280,000');
INSERT INTO hp_properties VALUES (366, 'Northam', '!!homestead On Acres...bring The Horses!!nestled On The Edge Of Town Yet Only A Few Minutes Drive From The Town Centre Is This Country Homestead Of Three Bedrooms And Family Bathroom On 4.93 Acres. The Large Country Kitchen Is The Hub Of ...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820253', 'http://images.domain.com.au/img/2011211/15440/2008820253_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 02:40:09', '0');
INSERT INTO hp_properties VALUES (367, 'Price Guide Over $780,000 - 3 Church Street Blakehurst', 'Alluring Family Home Of Light And Spacefull Of Warmth And Charm, This Bright And Breezy Home Offers Single Level Interiors That Offer A Relaxing Lifestyle In A Very Convenient Location. It Features A Choice Of Outdoor Living Areas To Enjoy...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820252', 'http://images.domain.com.au/img/2011211/13734/2008820252_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 02:22:09', '780,000');
INSERT INTO hp_properties VALUES (368, '49 Lisgar Street Merrylands', 'Fantastic Family Home With Two Street Frontagespresenting An Exceptional Opportunity For Investors And Young Families This Modern Family Home Has Two Street Frontages. It Provides The Option To Build A Second Home At The Rear With Its Own...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820249', 'http://images.domain.com.au/img/2011211/15092/2008820249_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 02:18:34', '49');
INSERT INTO hp_properties VALUES (369, 'Price Guide Over $1,450,000 - 19 Glenferrie Avenue Cremorne Point', 'Period Details And Tasteful Updates On 756sqmtucked Away From The Street Amid Tranquil Gardens, This 756sqm Home Occupies A Key Address With Direct Access To Harbourside Walks And Five Minutes Stroll To The Ferry. The Well Maintained C1915...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820244', 'http://images.domain.com.au/img/2011211/17242/2008820244_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 02:14:12', '1,450,000');
INSERT INTO hp_properties VALUES (370, 'Price Guide Over $1,200,000 - 43 Mort Street Balmain', 'A Modern Masterpiece In A Traditional Forma Modern Interpretation Of A Classic Form, This Impressive Tri-level Terrace Has Been Cleverly Renovated With An Exacting Attention To Detail To Create A Designer Home Of Style And Substance In One...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820238', 'http://images.domain.com.au/img/2011211/2717/2008820238_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 02:10:07', '1,200,000');
INSERT INTO hp_properties VALUES (371, '23 Spring Street Birchgrove', 'Character Semi-detached Victorian Residenceperfectly Positioned In A Premier Peninsula Location, This Spacious Home Is Offered For The First Time In Over 50 Years, Is In Immaculate Condition Yet Offers Tremendous Scope For Modernisation. ...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820239', 'http://images.domain.com.au/img/2011211/2717/2008820239_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 02:10:38', '23');
INSERT INTO hp_properties VALUES (372, 'Price Guide Over $315,000 - 15 Elkhorn Close Wyoming', 'Immaculately Presented Throughoutbright Interiors, A User-friendly Layout And Private Rear Yard Make This Brick Home An Excellent Start To Family Life. Positioned In A Quiet Street Setting And A Great Family-friendly Neighbourhood, It Enjo...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820233', 'http://images.domain.com.au/img/2011211/15690/2008820233_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 02:05:43', '315,000');
INSERT INTO hp_properties VALUES (373, 'Price Guide Over $323,000 - 52 Collareen Street Ettalong Beach', 'Ideal Location, Great Weekender Or Investmentthis Amazing Investment Opportunity Offers Dual Income With A Home, Cabin And Rear Lane Access. A Level Block Within Close Proximity To Shops, Schools, Beaches And Transport Offers Renovation Po...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008797391', 'http://images.domain.com.au/img/201121/16834/2008797391_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 02:05:10', '323,000');
INSERT INTO hp_properties VALUES (374, 'Price Guide Over $1,300,000 - 31 Rae Street Randwick', 'Character, Style, Convenience And Possibilitycleverly Balancing Traditional Character With An Understated Modern Style, This Inviting Freestanding Residence Presents An Affordable Entry Into A Highly Desirable Community Setting. Suited To ...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820231', 'http://images.domain.com.au/img/2011211/205/2008820231_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 02:03:21', '1,300,000');
INSERT INTO hp_properties VALUES (375, 'Price Guide Over $840,000 - 11 Lorna Avenue Blakehurst', 'Cosy Family Home Full Of Future Promiseoccupying 619sqm Of Sun Bathed North Facing Grounds, This Much Loved Spacious Home Offers A Great Option For The Expanding Family. It Provides A Relaxed And Light Filled Interior With Excellent Scope ...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820222', 'http://images.domain.com.au/img/2011211/17567/2008820222_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 01:52:51', '840,000');
INSERT INTO hp_properties VALUES (376, '9 The Panorama Eaglemont', 'Dress Circle Locationthe Panorama Views Looking For That Special Home Or Site? Boasting Land Size Over 950m2 Elevated Deep Allotment On The High Side With Absolutely Magnificent Views. Enjoy A Unique Older Style 3 Bedroom Fami...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008782881', 'http://images.domain.com.au/img/2011121/5699/2008782881_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 00:30:22', '9');
INSERT INTO hp_properties VALUES (377, '$270,000 Plus Buyers - 34 Pakenham Road Cockatoo', 'Peaceful Outlook On 2/3 Of An Acreperfectly Positioned To Take Advantage Of A Picturesque Outlook Is This Neat Brick Home. Inside Boasts 3 Bedrooms, Gas Heating, Timber Kitchen That Overlooks The Lounge Rooms & Meals Area. Outside Feature...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008699219', 'http://images.domain.com.au/img/20101119/11993/2008699219_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-11 00:00:00', '270,000');
INSERT INTO hp_properties VALUES (378, 'Auction - 184 President Avenue Brighton-le-sands', 'Great Re-build Opportunitygot A Home In Mind? Just Waiting For The Right Block Of Land To Put It On? This Could Be Just What You''re Looking For! This Rare Opportunity Rests On An Approximate 483sqm (12.19mx39.62m) Block Of Land With Rea...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008792032', 'http://images.domain.com.au/img/2011127/15729/2008792032_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-10 23:51:25', '0');
INSERT INTO hp_properties VALUES (379, '$189,000 - 28 Holden Street Camperdown', 'Beautifully Restored And Ready To Move In!this 3 Bedroom Home On A Large Block Has Been Beautifully Restored With A Great Focus On Detail, Quality Fixtures And Fittings, And Every Aspect Of It Complete. A Gravelled Driveway Leads Throug...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820209', 'http://images.domain.com.au/img/2011210/17918/2008820209_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-10 23:50:24', '189,000');
INSERT INTO hp_properties VALUES (380, '$475,000 - $485,000 - 64a Balcombe Avenue Findon', 'Quality Executive Living At It''s Absolute Finest - Stylish And Ultra Modern Too!this 2008 Torrens Title Rendition Built Home Not Only Oozes Workmanship But Class As Well. Built With Quality Fixtures & Fittings, It Is Ideally Located And Of...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820070', 'http://images.domain.com.au/img/2011210/7703/2008820070_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-10 23:40:01', '485,000');
INSERT INTO hp_properties VALUES (381, 'From $350,000 - 61 Camberwell Road Balga', '2 Units Available! Units A & B Available. Brand New Domination Brick & Tile Villa''s. Features Remote Garage, Good Size Courtyard, Quality Fit Out With Fisher & Paykel Appliances, 3 Good Size Bedrooms, Master With Ensuite And Bir, The S...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820197', 'http://images.domain.com.au/img/2011210/13208/2008820197_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-10 23:33:02', '350,000');
INSERT INTO hp_properties VALUES (382, '369000 - 7 Wingello Cres Wyoming', 'Easy To Live In - Easy To Letquality And Style Is Offered With This Spacious And Near New 3 Bedroom Townhouse With A Private Courtyard And Overlooking A Natural Bush Landscape. Located In A Suburb Within Easy Reach To The Best Of All The C...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820194', 'http://images.domain.com.au/img/2011210/15175/2008820194_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-10 23:32:28', '369,000');
INSERT INTO hp_properties VALUES (383, '399000 - 48 Bourke Ave Yattalunga', 'Fantastic First Homethis Extremely Affordable And Functional Home Is Funky And Full Of Character; It Is Located Within Easy Reach Of The Coast''s Best Beaches And Only A Stone''s Throw From The Waterfront Of Brisbane Water. Prepare To Be Im...', 'http://www.domain.com.au/Public/PropertyDetails.aspx?adid=2008820195', 'http://images.domain.com.au/img/2011210/15175/2008820195_1_TE.JPG', '2011-02-10 17:00:32', '2011-02-10 23:32:31', '399,000');


--
-- Data for Name: hp_properties_queries; Type: TABLE DATA; Schema: public; Owner: hurl
--



--
-- Data for Name: hp_queries; Type: TABLE DATA; Schema: public; Owner: hurl
--

INSERT INTO hp_queries VALUES (22, 'a:7:{s:6:"%mode%";s:3:"buy";s:7:"%state%";N;s:8:"%suburb%";s:6:"Darwin";s:11:"%proptypes%";s:6:"Studio";s:6:"%beds%";s:1:"5";s:6:"%from%";s:6:"100000";s:4:"%to%";i:5000000;}', '2011-02-10 17:05:46');


--
-- Data for Name: hp_users; Type: TABLE DATA; Schema: public; Owner: hurl
--

INSERT INTO hp_users VALUES (1, 'admin', 'nico@webpolis.com.ar', 'Nicolas', 'Iglesias', '1b9b94b45530d359ab28f5eb770d0a7a');


--
-- Data for Name: hp_users_pages; Type: TABLE DATA; Schema: public; Owner: hurl
--



--
-- Name: pk_hp_pages_id; Type: CONSTRAINT; Schema: public; Owner: hurl; Tablespace: 
--

ALTER TABLE ONLY hp_pages
    ADD CONSTRAINT pk_hp_pages_id PRIMARY KEY (id);


--
-- Name: pk_hp_properties; Type: CONSTRAINT; Schema: public; Owner: hurl; Tablespace: 
--

ALTER TABLE ONLY hp_properties
    ADD CONSTRAINT pk_hp_properties PRIMARY KEY (id);


--
-- Name: pk_hp_properties_queries; Type: CONSTRAINT; Schema: public; Owner: hurl; Tablespace: 
--

ALTER TABLE ONLY hp_properties_queries
    ADD CONSTRAINT pk_hp_properties_queries PRIMARY KEY (property_id, query_id);


--
-- Name: pk_hp_queries; Type: CONSTRAINT; Schema: public; Owner: hurl; Tablespace: 
--

ALTER TABLE ONLY hp_queries
    ADD CONSTRAINT pk_hp_queries PRIMARY KEY (id);


--
-- Name: pk_users_id; Type: CONSTRAINT; Schema: public; Owner: hurl; Tablespace: 
--

ALTER TABLE ONLY hp_users
    ADD CONSTRAINT pk_users_id PRIMARY KEY (id);


--
-- Name: uk_hp_users_pages_user_id_page_id; Type: CONSTRAINT; Schema: public; Owner: hurl; Tablespace: 
--

ALTER TABLE ONLY hp_users_pages
    ADD CONSTRAINT uk_hp_users_pages_user_id_page_id UNIQUE (user_id, page_id);


--
-- Name: fk_hp_properties_queries_ref_hp_queries; Type: FK CONSTRAINT; Schema: public; Owner: hurl
--

ALTER TABLE ONLY hp_properties_queries
    ADD CONSTRAINT fk_hp_properties_queries_ref_hp_queries FOREIGN KEY (query_id) REFERENCES hp_queries(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fk_hp_properties_ref_hp_properties; Type: FK CONSTRAINT; Schema: public; Owner: hurl
--

ALTER TABLE ONLY hp_properties_queries
    ADD CONSTRAINT fk_hp_properties_ref_hp_properties FOREIGN KEY (property_id) REFERENCES hp_properties(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

