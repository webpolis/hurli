<?php

Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'front'));
Router::connect('/my-hunt', array('controller' => 'pages', 'action' => 'display', 'hunt'));
Router::connect('/shortlist', array('controller' => 'pages', 'action' => 'display', 'shortlist'));
Router::connect('/my-planner', array('controller' => 'pages', 'action' => 'display', 'planner'));

Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));
Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
Router::connect('/forgot', array('controller' => 'users', 'action' => 'forgot'));
Router::connect('/register', array('controller' => 'users', 'action' => 'add'));
Router::connect('/users/captcha_image/*', array('controller' => 'users', 'action' => 'captcha_image'));

Router::connect('/search', array('controller' => 'properties', 'action' => 'search'));