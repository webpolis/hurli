<?php

define('SCRAPER_MAX_IMG_ITERATE', 5);

Configure::write('default_query', 'Enter Place or Postcode');

Configure::write('domain_com_au_settings', array(
            'urls' => array(
                'http://www.domain.com.au/Ore/Feeds/SearchResultsRSS.ashx?mode=%mode%&state=%state%&sub=%suburb%&proptypes=%proptypes%&bedrooms=%beds%&from=%from%&to=%to%'
            ),
            'proptypes' => array(
                'House' => 1,
                'Floor' => 2,
                'Studio' => 3
            ),
            'rss' => true
        ));

Configure::write('realstate_com_au_settings', array(
            'urls' => array(
                'http://www.realestate.com.au/%mode%/property-%proptypes%-with-%beds%-bedrooms-between-%from%-%to%-in-%suburb%/list-1?'
            ),
            'proptypes' => array(
                'House' => 'house',
                'Floor' => 'apartment',
                'Studio' => 'unit-with-studio'
            )
        ));
