<?php
class AppError extends ErrorHandler{

    function notAllowed() {
        $this->_outputMessage('not_allowed');
    }

    function admin() {
        $this->_outputMessage('admin');
    }

    function notFound() {
        $this->_outputMessage('not_found');
    }
}
?>
