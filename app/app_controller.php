<?php

class AppController extends Controller
{

    function  __construct()
    {
        $this->components[] = 'Auth';
        $this->helpers[] = 'Javascript';
        parent::__construct();
    }

    function beforeFilter()
    {
        $this->processUrl();
        Security::setHash('md5');
        $this->Auth->authenticate = ClassRegistry::init('User');
        $this->Auth->userModel = "User";
        $this->Auth->loginAction = array("controller" => "users", "manager" => false, "action" => "login");
        $this->Auth->logoutRedirect = "/";
        $this->Auth->loginRedirect = array("controller" => "pages", "manager" => false, "action" => "init");
        $this->Auth->allow(array("register", "load", "toggle",'display'));
        if ($this->Session->check("Auth.User")) {
            $is_admin = ($this->Session->check("Auth.User.admin")) ? (bool) $this->Session->read("Auth.User.admin") : false;
            $manager = (isset($this->params['manager']) && (bool) $this->params['manager']) ? true : false;
            if (!$is_admin && $manager) {
                $this->cakeError("admin");
            }
        }
        //$this->_redirect();
    }

    function _redirect()
    {
        if (isset($this->params['url']) && isset($this->params['url']['url'])) {
            $url = $this->params['url']['url'];
            $lhost = $_SERVER['HTTP_HOST'];
            $refhost = parse_url($this->referer());
            if (isset($refhost['host']) && strtolower($lhost) != strtolower($refhost['host'])) {
                $this->set("redirect", $url);
            }
        }
    }

    function processUrl()
    {
        if (isset($this->params['url']) && isset($this->params['url']['url'])) {
            $url = $this->params['url']['url'];
            $this->loadModel("Page");
            $p = & new Page();
            if ($p->hasAny(
                            array("url" => $url)
            )) {
                Router::connect("/{$url}", array("controller" => "pages",
                            "manager" => false, "action" => "load"));
            }
        }
    }

    function _getRandomWord()
    {
        $w = null;
        $c = array("a", "e", "i", "o", "u", "c", "d", "s", "t", "r", "h", "m", "n",
            "b", "v", "f", "p", "q", "l", "w", "x", "z", "y", "j", "k");
        $n = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        shuffle($c);
        shuffle($n);
        for ($i = 0; $i <= 4; $i++) {
            $w .= $c[$i];
        }
        $w = ((bool) rand(0, 1)) ? strtoupper($w) : $w;
        for ($i = 0; $i <= 4; $i++) {
            $w .= $n[$i];
        }
        return $w;
    }

    function toggle($field=null, $id=null)
    {
        $this->render(null, null, '/pages/reported');
        if (!empty($field) && !empty($id)) {
            $model = Inflector::singularize($this->name);
            if ($this->{$model}->hasField($field)) {
                $this->{$model}->id = $id;
                $this->{$model}->saveField($field, !$this->{$model}->field($field));
                $this->set("model", $model);
            }
        }
    }

}

?>