<div class="front">
    <div class="wrapper clearfix">
        <div class="content">
            <div class="span-20 last gui clear">
                <div class="top-x-line"></div>
                <div class="users form">
                    <h2>Registration</h2><br/>
                    <?php echo $this->Form->create('User'); ?>
                    <?php
                    echo $form->create("User", array("url" => "/register"));
                    echo $form->input("username",
                            array(
                                "autocomplete" => "off", "title" => "Username"));
                    echo $form->input("password",
                            array(
                                "autocomplete" => "off", "title" => "Password"));
                    echo $form->input("password_confirm",
                            array(
                                "autocomplete" => "off", "title" => "Confirm password", "type" => "password"));
                    echo $form->input("email",
                            array(
                                "autocomplete" => "off", "title" => "Email address"));
                    echo $this->Form->input('first_name',
                            array(
                                "autocomplete" => "off", "title" => "First name"));
                    echo $this->Form->input('last_name',
                            array(
                                "autocomplete" => "off", "title" => "Last name"));
                    echo $this->element("captcha");
                    echo $form->submit('Register', array("blank" => true, "div" => false));
                    echo $form->end();
                    ?>
                </div>
            </div>
            <div class="bg-bottom-shadow span-20 last clear"></div>
        </div>
    </div>
</div>
