<?php echo $this->element("admin_menu"); ?>
<div class="users form">
    <?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php printf(__('Manager Add %s', true), __('User', true)); ?></legend>
        <?php
        echo $this->Form->input('email');
        echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
        echo $this->Form->input('username');
        echo $this->Form->input('password',array("autocomplete"=>"off","value"=>false));
        echo $this->Form->input('password_confirm',array("type"=>"password","autocomplete"=>"off","value"=>false));
        echo $this->element("captcha");
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit', true)); ?>
</div>
