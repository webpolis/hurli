<?php echo $this->element("admin_menu"); ?>
<div class="users view">
    <h2><?php __('User'); ?></h2>
    <dl><?php $i = 0;
$class = ' class="altrow"'; ?>
        <dt<?php if ($i % 2 == 0)
    echo $class; ?>><?php __('Id'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
<?php echo $user['User']['id']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Username'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
            <?php echo $user['User']['username']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('First Name'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
<?php echo $user['User']['first_name']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Last Name'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
<?php echo $user['User']['last_name']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Picture'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
<?php echo $user['User']['picture']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Created'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
<?php echo $user['User']['created']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Updated'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
<?php echo $user['User']['updated']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Banned'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
<?php  echo ((bool) $user['User']['banned']) ? "Yes" : "No"; ?>
            &nbsp;
        </dd>
    </dl>
</div>
