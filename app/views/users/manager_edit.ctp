<?php echo $this->element("admin_menu"); ?>
<div class="users form">
    <?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php printf(__('Manager Edit %s', true), __('User', true)); ?></legend>
        <?php
        echo $this->Form->input('banned');
        echo $this->Form->input('id');
        echo $this->Form->input('email');
        echo $this->Form->input('username', array("disabled" => true));
        echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
        echo $this->Form->input('picture');
        echo $this->Form->input('password', array("autocomplete" => "off", "value" => false,
            "label" => "Password<br /><span class='mini'>Leave blank to keep existing password</span>"));
        echo $this->Form->input('password_confirm', array("type" => "password", "autocomplete" => "off", "value" => false));
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit', true)); ?>
</div>
