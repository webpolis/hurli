<div class="front">
    <div class="wrapper clearfix">
        <div class="content">
            <div class="span-20 last gui clear">
                <div class="top-x-line"></div>
                <?php
                echo $form->create("User", array("url" => "/login"));
                echo $form->input("username",
                        array("div" => false,
                            "label" => false, "autocomplete" => "off", "title" => "Username"));
                echo '<br />';
                echo $form->input("password",
                        array("div" => false,
                            "label" => false, "autocomplete" => "off", "title" => "Password"));
                echo '<br />';
                echo $form->submit("", array("blank" => true, "div" => false));
                echo $form->end();
                ?>
                <br />
                <?php echo $html->link("Forgot your password?",
                        array("controller" => "users", "action" => "forgot")); ?>
            </div>
            <div class="bg-bottom-shadow span-20 last clear"></div>
        </div>
        <div class="space"></div>
    </div>
</div>
