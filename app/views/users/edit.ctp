<div class="users form">
    <div id="status" style="display:none"><?php echo $html->image("loading.gif"); ?>
        <br />Updating your information, please wait...</div>
    <?php echo $this->Form->create('User', array("url" => "/account", "type" => "file")); ?>
    <fieldset>
        <legend><?php printf(__('Edit %s', true), __('your details', true)); ?></legend>
        <?php
        $img = (!empty($this->data['User']['picture']) &&
                file_exists(WWW_ROOT . "/img/users/" . $this->data['User']['picture'])) ?
                "users/" . $this->data['User']['picture'] :
                "users/undefined.jpg";
        echo $this->Form->input('id');
        echo $this->Form->input('email');
        echo $this->Form->input('username', array("disabled" => true));
        echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
        echo $this->Form->label(null, "Update your picture<br />" . $html->image($img) . "<br />", array("escape" => false));
        echo $this->Form->file('picture');
        echo $this->Form->input('password', array("autocomplete" => "off", "value" => false,
            "label" => "Password<br /><span class='mini'>Leave blank to keep existing password</span>"));
        echo $this->Form->input('password_confirm', array("type" => "password", "autocomplete" => "off", "value" => false));
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit', true), array("onclick" => "javascript:void($('#status').show());")); ?>
</div>
