<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <?php
            echo $this->Html->meta('icon');
            echo $this->Html->css(array('blueprint/src/reset',
                'blueprint/src/print', 'blueprint/screen', 'blueprint/plugins/fancy-type/screen',
                'blueprint/src/typography', 'sprite',
                'custom-theme/jquery-ui-1.8.8.custom',
                'custom-theme/jquery.ui.selectmenu', 'main'));
            echo $this->Javascript->link(array(
                'jquery-1.4.4.min', 'jquery-ui-1.8.8.custom.min',
                'jquery.ui.selectmenu', 'main'
            ));
            echo $scripts_for_layout;
        ?>
        </head>
        <body>
            <div class="root <?php echo isset($this->viewVars['page'])
                        ?
                    'page-' . $this->viewVars['page'] : ((isset($section))
                                ? $section : 'common'); ?>">
             <?php echo $this->element('header'); ?>
            <?php echo $this->element('middle', compact('content_for_layout')); ?>
            <?php echo $this->element('sql_dump'); ?>
        </div>
    </body>
</html>