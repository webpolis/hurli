<div class="main-nav">
    <div class="buttons span-15 last clearfix">
        <?php
        echo $this->Html->link(
                $this->Html->div('sprite sprite-btn-hunt', false)
                , '/my-hunt', array('escape' => false));
        echo $this->Html->link(
                $this->Html->div('sprite sprite-btn-shortlist', false)
                , '/shortlist', array('escape' => false));
        echo $this->Html->link(
                $this->Html->div('sprite sprite-btn-planner', false)
                , '/my-planner', array('escape' => false));
        ?>
    </div>
</div>