<div class="top-line"></div>
<div class="header">
    <div class="prepend-18 last top-options">
        <div class="sprite sprite-top-share-this"></div>
    </div>
    <div class="span-20 last">
        <?php
        echo $this->Html->link(
                $this->Html->div('span-4 sprite sprite-logo', false),
                '/', array('escape' => false));
        ?>
    </div>
    <div class="span-10 append-10 last push-2">
        <div class="sprite sprite-logo-legend"></div>
    </div>
</div>
