<?php
$paginator->options(array('url' => $query_id));
?>
<div class="front">
    <div class="wrapper clearfix">
        <div class="content">
            <div class="span-20 last gui clear">
                <div class="top-x-line"></div>
                <div class="results">
                    <h3 class="query"><?php echo __('Search Results') . ': '
 . '<span>' . $this->data['Property']['query'] . '</span>'; ?></h3>
                    <?php
                    if ((isset($items, $items['Property']))
                            || (isset($items['0'], $items['0']['Property'])) && !empty($items)) {
                        $nested = isset($items['0'], $items['0']['Property'])
                                    ? true : false;
                        $properties = $nested ? $items : $items['Property'];
                        foreach ($properties as $property) {
                            $property = $nested ? $property['Property'] : $property;
                    ?>
                            <div class="property">
                                <span class="thumb"><?php echo $html->image($property['image']); ?></span>
                                <span class="details">
                                    <h4><?php echo $property['title']; ?></h4>
                                    <div class="clear"></div>
                                    <span class="price">$ <?php echo $property['price']; ?></span>
                            <?php
                            echo $html->link(__('view details', array('return')), '#');
                            ?>
                            <div class="options">
                                <?php
                                echo $html->link($html->div('sprite sprite-btn-add-shortlist', false),
                                        '#', array('escape' => false));
                                echo $html->link($html->div('sprite sprite-btn-add-notes', false),
                                        '#', array('escape' => false));
                                ?>
                            </div>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                    <?php
                            }
                        } else {
                            echo $html->para('notice', __('No results were found. Please, refine your search.',
                                            array('return')));
                        }
                    ?>
                    <?php
                        echo $paginator->prev('« Previous ', null, null, array('class' => 'disabled'));
                        echo $paginator->numbers();
                        echo $paginator->next(' Next »', null, null, array('class' => 'disabled'));
                    ?>
                </div>
            </div>
            <div class="bg-bottom-shadow span-20 last clear"></div>
        </div>
        <div class="space"></div>
    </div>
</div>

