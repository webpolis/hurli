<?php

echo $form->create('search', array(
    'url' => '/search',
    'id' => 'PropertySearch',
    'type' =>'GET'
));
echo $this->element('cbox', array(
    'content' => $form->input('Property.query', array('div' => false, 'label' => false,
        'value' => __(Configure::read('default_query'), 'return')))
));
$types = $form->label('Property.type', __('Choose type', 'return') . ':');
$types .= $this->element('cbox', array(
            'content' => $form->radio('Property.type',
                    ClassRegistry::init('Property')->getTypes()
                    , array('legend' => false, 'div' => false,
                'class' => 'types'))
        ));

echo $html->div('prepend-top span-19', $types);

$choices = $form->label('Property.beds', __('Number of Beds', 'return'));
$choices .= $form->select('Property.beds', ClassRegistry::init('Property')->getBeds());
$choices .= $form->label('Property.from', __('Budget [$]', 'return'));
$choices .= $form->select('Property.from', ClassRegistry::init('Property')->getBudget());
$choices .= $form->label('Property.upto', __('up to', 'return'));
$choices .= $form->select('Property.upto', ClassRegistry::init('Property')->getBudget());

echo $html->div('choices push-4 span-15 prepend-top append-bottom last',
        $choices
);
echo $form->end();
?>