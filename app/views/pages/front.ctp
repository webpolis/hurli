<div class="front">
    <div class="wrapper clearfix">
        <div class="content">
            <div class="span-20 last gui clear">
                <div class="top-x-line"></div>
                <?php
                echo $this->render('/properties/search', 'ajax');
                ?>
            </div>
            <div class="bg-bottom-shadow span-20 last clear"></div>
        </div>
        <div class="space"></div>
        <div class="span-14">
            <div class="sprite sprite-bg-info"></div>
        </div>
        <div class="span-6 last">
            <?php
                echo $html->link($html->div('sprite sprite-btn-get-started',
                                false)
                        , 'javascript:void($("#PropertySearch").submit())'
                        , array('escape' => false));
            ?>
            </div>
            <div class="clear prepend-top span-20 last"></div>
            <div class="span-20 last pull-1 prepend-top">
                <div class="sprite sprite-bg-down-arrow"></div>
            </div>
            <div class="span-11 prepend-top">
            <?php
                echo $this->Html->link($html->div('sprite sprite-txt-new-here', false),
                        '/register', array('escape' => false));
            ?>
            </div>
            <div class="span-3 prepend-top prepend-2">
                <div class="sprite sprite-txt-tour"></div>
            </div>
            <div class="span-3 last prepend-top prepend-1">
                <div class="sprite sprite-txt-demo"></div>
            </div>
        </div>
    </div>
<?php
                echo $javascript->codeBlock("
$(function(){
    init('front');
});
");
?>