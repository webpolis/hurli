function init(page){
    if(typeof(page)!='undefined'){
        switch(page){
            case 'front':
                $('input.types').parent().buttonset();
                window.inputPropertyQueryDefaultValue = $('#PropertyQuery').attr('value');
                $('#PropertyQuery').click(function(){
                    if($(this).attr('value')==window.inputPropertyQueryDefaultValue){
                        $(this).attr('value','');
                    }
                }).blur(function(){
                    if($(this).attr('value')==''){
                        $(this).attr('value',window.inputPropertyQueryDefaultValue);
                    }
                });
                $('select').selectmenu();
                break;
        }
    }
}