<?php

class TextCoding
{

    public static function getNormalized($text=null)
    {
        $ret = null;
        if (!empty($text)) {
            $encoding = mb_detect_encoding($text, "UTF-8,ISO-8859-1,WINDOWS-1252");
            $ret = (!stristr($encoding, "UTF-8")) ? utf8_encode($text) : $text;
        }
        return ucwords(strtolower(preg_replace('/\s{1,}/si', ' ', trim($ret))));
    }

}

?>
