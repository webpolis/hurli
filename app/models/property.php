<?php

class Property extends AppModel
{

    public $name = 'Property';
    protected $types = array(
        'House' => 'House',
        'Flat' => 'Flat',
        'Bungalow' => 'Bungalow',
        'Floor' => 'Floor',
        'Studio' => 'Studio',
        'Condo' => 'Condo',
    );
    protected $max_beds = 5;
    protected $max_budget = 5000000;
    protected $rss_keys = array(
        'title',
        'description',
        'link',
        'pubDate'
    );
    protected $scenario = null;
    public $hasAndBelongsToMany = array(
        'Query' => array(
            'className' => 'Query',
            'joinTable' => 'properties_queries',
            'unique' => true
        )
    );

    public function getRssKeys()
    {
        return $this->rss_keys;
    }

    public function getTypes()
    {
        return $this->types;
    }

    public function getMaxBudget()
    {
        return $this->max_budget;
    }

    public function getBudget()
    {
        $range = range(0, $this->max_budget, 50000);
        return array_combine(array_values($range), array_values($range));
    }

    public function getBeds()
    {
        $range = range(1, $this->max_beds);
        return array_combine(array_values($range), array_values($range));
    }

    public function getDomainComAuPropertyType($key=null)
    {
        if (!empty($key)) {
            $domain_com_au_settings = Configure::read('domain_com_au_settings');
            return isset($domain_com_au_settings['proptypes'][$key]) ?
                    $domain_com_au_settings['proptypes'][$key] :
                    implode(',', $domain_com_au_settings['proptypes']);
        }
    }

    public function getRealstateComAuPropertyType($key=null)
    {
        if (!empty($key)) {
            $realstate_com_au_settings = Configure::read('realstate_com_au_settings');
            return isset($realstate_com_au_settings['proptypes'][$key]) ?
                    $realstate_com_au_settings['proptypes'][$key] :
                    'unit';
        }
    }

    /**
     * Returns array of items in regular format so they can be saved
     * @param <type> $items
     * @return array
     */
    public function parseItems(&$items=null)
    {
        App::import('Lib', 'TextCoding');

        $ret = array();
        if (!empty($items)) {
            foreach ($items as $i) {
                $ret[] = array(
                    'Property' => array(
                        'title' => TextCoding::getNormalized($i['title']),
                        'price' => (string) number_format((double)
                                preg_replace(array('/^.*(?<=\\$|\\$\s)([\d\,\.]+).*$/s', '/[\.\,]+/s',
                                    '/\s{1,}/s'), array('$1', '', ''),
                                        ((isset($i['price'])) ? $i['price']
                                                    : $i['title'])),
                                0, '.', ','),
                        'pub_date' => $i['pubDate'],
                        'link' => (isset($i['link']) ? $i['link'] : null),
                        'description' => TextCoding::getNormalized(strip_tags($i['description'])),
                        'image' => ((isset($i['image'])) ? $i['image'] :
                                preg_replace('/^.*\<img[^\>]+src\=[\"\']([^\"\']+)[\"\'][^\>]+\>.*$/si', '$1',
                                        trim($i['description']))
                        )
                    )
                );
            }
        }
        return $ret;
    }

    public function setScenario($scenario=null)
    {
        $this->scenario = $scenario;
    }

    public function beforeSave()
    {
        if ($this->scenario === 'persist_search') {
            App::import('Component', 'Session');
            $session = & new SessionComponent();

            if ($session->check('Query.id')) {
                $this->data['Query']['id'] = $session->read('Query.id');
            }
        }
        return true;
    }

}

?>
