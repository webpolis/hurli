<?php

class Query extends AppModel
{

    public $name = 'Query';

    public $hasAndBelongsToMany = array(
        'Property' => array(
            'className' => 'Property',
            'joinTable' => 'properties_queries',
            'unique' => true
        )
    );
}

?>
