<?php

class Page extends AppModel
{

    var $name = 'Page';
    var $validate = array(
        'url' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            "unique" => array(
                'rule' => array("unique", "url"),
                'message' => "This hurl is already in use",
                "on" => "update"
            )
        ),
        'password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'You cannot leave this field empty',
                'on' => "create"
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'password_confirm' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please confirm your password'
            ),
            'identicalFieldValues' => array(
                'rule' => array('identicalFieldValues', 'password'),
                'message' => 'Passwords do not match',
                'on' => 'create'
            )
        ),
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasAndBelongsToMany = array(
        'User' => array(
            'className' => 'User',
            'joinTable' => 'users_pages',
            'foreignKey' => 'page_id',
            'associationForeignKey' => 'user_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => ''
        )
    );

    function identicalFieldValues($field=array(), $compare_field=null)
    {
        $data = $this->data[$this->name];
        foreach ($field as $value) {
            if (!isset($data[$compare_field])) {
                return true;
            }
            $v = $data[$compare_field];
            if (md5($value) !== $v) {
                return false;
            }
        }
        return true;
    }

    function beforeSave()
    {
        if(!isset($this->data['Page']['public'])){
            return true;
        }
        if ((bool) $this->data['Page']['public']) {
            return true;
        }
        return false;
    }

    function unique($value, $field="username")
    {
        if (!empty($value)) {
            return!$this->find("first", array("conditions" => array("{$this->name}.$field" => $value)));
        }
        return true;
    }

}
?>