<?php

class User extends AppModel
{

    var $name = 'User';
    var $displayField = 'username';
    var $validate = array(
        'username' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'This field cannot be left empty'
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            "unique" => array(
                'rule' => array("unique", "username"),
                'message' => "This user name is already in use",
                "on" => "create"
            )
        ),
        'email' => array(
            'email' => array(
                'rule' => array('email'),
                'message' => 'This is not a valid email address'
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            "unique" => array(
                'rule' => array("unique", "email"),
                'message' => "This email address is already in use",
                "on" => "create"
            )
        ),
        'first_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'This field cannot be left empty'
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'last_name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'This field cannot be left empty'
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'You cannot leave this field empty',
                'on' => "create"
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'password_confirm' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please confirm your password'
            ),
            'identicalFieldValues' => array(
                'rule' => array('identicalFieldValues', 'password'),
                'message' => 'Passwords do not match',
            )
        ),
        'captcha' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'You must copy the verification code from the above image',
                'on' => "create",
                'required'=>true
            ),
            'validateCaptcha' => array(
                'rule' =>'validateCaptcha',
                'message' => 'The verification code is incorrect',
                'on' => "create"
            )
        ),
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $hasMany = array(
        'Page' => array(
            'className' => 'Page',
            'foreignKey' => 'user_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    var $hasAndBelongsToMany = array(
        'Page' => array(
            'className' => 'Page',
            'joinTable' => 'users_pages',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'page_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => ''
        )
    );

    function unique($value, $field="username")
    {
        if (!empty($value)) {
            return!$this->find("first", array("conditions" => array("User.$field" => $value)));
        }
        return true;
    }

    function identicalFieldValues($field=array(), $compare_field=null)
    {
        $data = $this->data[$this->name];
        foreach ($field as $value) {
            if (!isset($data[$compare_field])) {
                return true;
            }
            $v = $data[$compare_field];
            if ($value !== $v) {
                unset($auth);
                return false;
            }
        }
        return true;
    }

    function validateCaptcha($fieldName, $params)
    {
        $arr = array_values($fieldName);
        $val = $arr['0'];
        $caseInsensitive = true;
        if ($caseInsensitive) {
            $val = strtoupper($val);
        }
        if (!defined('CAPTCHA_SESSION_ID'))
            define('CAPTCHA_SESSION_ID', 'php_captcha');
        if (!empty($_SESSION[CAPTCHA_SESSION_ID]) && $val == $_SESSION[CAPTCHA_SESSION_ID]) {
            // clear to prevent re-use
            unset($_SESSION[CAPTCHA_SESSION_ID]);
            return true;
        }
        return false;
    }

}
?>