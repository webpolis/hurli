<?php

/**
 * Scraps a website
 *
 * @author Nicolas Iglesias <nico@webpolis.com.ar>
 */
App::import('Vendor', 'simplehtmldom/simple_html_dom');
App::import('Lib', 'TextCoding');

class ScraperComponent extends Object
{

    protected $controller = null;
    protected $settings = array();
    protected $items = array();
    private $url = null;
    private $host = null;
    public $name = 'Scraper';

    public function initialize(&$controller=null, &$settings=array())
    {
        $this->controller = & $controller;
        $this->settings = & $settings;
    }

    public function setup(&$settings = array(), &$query=array())
    {
        $this->items = array();
        $this->settings = & $settings;
        $k = array_keys($query);
        array_walk($k, create_function('&$v,$k', '$v = "/".str_replace("%","%",$v)."/si";'));
        if (!empty($this->settings['urls'])) {
            foreach ($this->settings['urls'] as $ix => $url) {
                $this->settings['urls'][$ix] = preg_replace($k, array_values($query), $url);
            }
        }
    }

    public function scrap($domain=null)
    {
        $this->text = array();
        $this->img = array();
        if (!empty($this->settings['urls'])) {
            foreach ($this->settings['urls'] as $url) {
                $url_original = $url; // store same url so JS will SHA1 accordignly
                $url = (!preg_match("/^https?:\/\/.*/si", $url)) ? "http://" . $url
                            : $url;
                
                if (!empty($url)) {
                    $this->url = preg_replace("/^(.*)\/?$/si", "$1", $url);
                    $this->host = preg_replace("/^(?:https?:\/\/([^\/]+)).*$/si", "$1", $url);
                    $this->imagesFolder = WWW_ROOT . "img/pages/" . sha1($url_original) . "/";

                    if ((isset($this->settings['rss']) && !$this->settings['rss']) || !isset($this->settings['rss'])) {
                        try {
                            $headers = array(
                                'http' => array(
                                    'method' => "GET",
                                    'header' =>
                                    "User-Agent: Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.3) Gecko/20100423 Ubuntu/10.04 (lucid) Firefox/3.6.3\r\n"
                                    . "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3\r\n"
                                    . "Accept-language: en\r\n"
                                    . "Accept: application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\n"
                                    . "Host: {$this->host}"
                                )
                            );

                            $ctx = stream_context_create($headers);
                            $str = file_get_contents($this->url, FILE_TEXT | FILE_SKIP_EMPTY_LINES, $ctx);
                            $html = new simple_html_dom;
                            $html->load($str, false);

                            switch ($domain) {
                                case 'realestate.com.au':
                                    foreach ($html->find('div[class=resultBody]') as $item) {
                                        $i = array();
                                        $i['title'] = $item->find('div[class=descriptionCont]', 0)->find('h3[class=title]',0)->plaintext;
                                        $i['price'] = $item->find('div[class=propertyStats]', 0)->find('p[class=price]',0)->plaintext;
                                        $i['price'] = preg_replace('/.*(\$[\d\,\s\.]+)\s*.*/si','$1',$i['price']);
                                        $i['description'] = $item->find('div[class=descriptionCont]', 0)->find('p[class=description]',0)->plaintext;
                                        $i['pubDate'] = date('Y-m-d H:i:s');
                                        $i['image'] = $item->find('div[class=photoviewer] img', 0)->getAttribute('src');
                                        $i['link'] = null;
                                        $this->items[] = $i;
                                    }
                                    break;
                            }

                            $html->clear();
                            unset($html);
                        } catch (Exception $e) {

                        }
                    } else if (isset($this->settings['rss']) && $this->settings['rss']) {
                        $this->_processRss();
                    }
                }
            }
            $this->img = array_filter($this->img);
        }
    }

    public function getItems()
    {
        return $this->items;
    }

    private function _processRss()
    {
        App::import('Xml');
        $xml = & new Xml($this->url);
        $ret = $xml->toArray();
        $items = isset($ret['Rss'], $ret['Rss']['Channel'], $ret['Rss']['Channel']['Item'])
                    ?
                $ret['Rss']['Channel']['Item'] : null;
        $this->items = $items;
    }

}

?>
