<?php

class PropertiesController extends AppController
{

    private $items = array();
    var $paginate = array(
        'Property' => array(
            'limit' => 5,
            'joins' => array(
                array(
                    'table' => 'hp_properties_queries',
                    'alias' => 'PropertiesQueries',
                    'type' => 'inner',
                    'conditions' => array('PropertiesQueries.property_id = Property.id')
                ),
                array(
                    'table' => 'hp_queries',
                    'alias' => 'Query',
                    'type' => 'inner',
                    'conditions' => array(
                        'Query.id = PropertiesQueries.query_id'
                    )
            ))
        )
    );

    function __construct()
    {
        $this->helpers[] = 'Javascript';
        $this->components[] = 'Scraper';
        $this->components[] = 'Session';
        parent::__construct();
    }

    public function beforeFilter()
    {
        $this->Auth->allow(array('search'));
        parent::beforeFilter();
    }

    public function search($qid=null)
    {
        $this->items = array();
        $query_id = isset($qid) ? $qid : null;
        $domain_com_au_items = array();
        $this->data['Property'] = $_GET;
        if (!empty($this->data) && isset($this->data['Property']) && !isset($query_id)) {
            $q = $this->data['Property']['query'] !== Configure::read('default_query')
                        ? explode(',', $this->data['Property']['query']) : null;

            if (!empty($q)) {
                $domain_com_au_settings = Configure::read('domain_com_au_settings');
                $realstate_com_au_settings = Configure::read('realstate_com_au_settings');

                /**
                 * DOMAIN.COM.AU
                 */
                $query = array(
                    '%mode%' => 'buy',
                    '%state%' => isset($q['1']) ? $q['1'] : null,
                    '%suburb%' => $q['0'],
                    '%proptypes%' => $this->data['Property']['type'],
                    '%beds%' => (isset($this->data['Property']['beds']) &&
                    !empty($this->data['Property']['beds']) ?
                            $this->data['Property']['beds'] : '5'),
                    '%from%' => isset($this->data['Property']['from'])
                    && !empty($this->data['Property']['from']) ?
                            $this->data['Property']['from'] : '0',
                    '%to%' => isset($this->data['Property']['to']) ?
                            $this->data['Property']['to'] : $this->Property->getMaxBudget(),
                );

                $search = serialize($query);
                if (!$this->Property->Query->hasAny(array('query' => $search))) {

                    // scrap domain.com.au
                    $query['%proptypes%'] = (string) $this->Property->getDomainComAuPropertyType($this->data['Property']['type']);
                    $this->Scraper->setup($domain_com_au_settings, $query);
                    $this->Scraper->scrap();
                    $domain_com_au_items = $this->Scraper->getItems();
                    $domain_com_au_items = $this->Property->parseItems($domain_com_au_items);
                    $this->items = array_merge($domain_com_au_items, $this->items);

                    // scrap realstate.com.au
                    $query['%proptypes%'] = $this->Property->getRealstateComAuPropertyType($this->data['Property']['type']);
                    $this->Scraper->setup($realstate_com_au_settings, $query);
                    $this->Scraper->scrap('realestate.com.au');
                    $realstate_com_au_items = $this->Scraper->getItems();
                    $realstate_com_au_items = $this->Property->parseItems($realstate_com_au_items);
                    $this->items = array_merge($realstate_com_au_items, $this->items);

                    // persist search and results
                    $this->persistSearch($search);
                }

                // retrieve cached results
                $query_id = $this->Property->Query->field('id', array('query' => $search));
            }
        }

        if (!empty($query_id)) {
            $this->Property->Query->id = $query_id;
            $qdata = unserialize($this->Property->Query->field('query'));
            $this->data['Property']['query'] = $qdata['%suburb%'];
            $this->Property->recursive = 1;
            $items = $this->paginate('Property', array('Query.id' => $query_id));
        }

        $this->set(compact('items', 'query_id'));
        $this->set('section', 'page-hunt');
        $this->render('results');
    }

    private function persistSearch(&$search=null)
    {
        $query_id = null;

        if (!empty($search)) {
            $this->Property->Query->save(array('Query' => array('query' => $search)));
            $query_id = $this->Property->Query->getLastInsertId();
        }

        // store query in session for further reference (Property's beforeSave)
        $this->Session->write('Query.id', $query_id);

        // save items
        if (!empty($this->items)) {
            $this->Property->setScenario('persist_search');
            $this->Property->saveAll($this->items);
        }
    }

}

?>
