<?php

class PagesController extends AppController
{

    var $name = 'Pages';
    var $url = null;
    var $host = null;
    var $imagesFolder = null;
    var $common_words = array();

    function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(array("index", "display", "scraper", "saveOrder", "sites",
            "add", "view", "init", "create", "unlock", "add_favorite", "favorites", "get_stats",
            "hurls_list"));
    }

    function favorites($search=null)
    {
        $this->layout = "ajax";
        $favorites = array();
        $conds = ($this->Session->check("current_page")) ?
                array("Page.id" => $this->Session->read("current_page")) : array("Page.url" => $this->Session->read("master"),
            "Page.master" => $this->Session->read("master"));
        $t = $this->Page->find("first", array(
                    "fields" => array("Page.id"),
                    "conditions" => $conds
                ));
        $this->set("page_id", $t['Page']['id']);
        unset($t);
        if ($this->Session->check("Auth.User")) {
            $tmp = array();
            $ids = $this->Page->UsersPage->find("all", array(
                        "fields" => array("page_id", "title"),
                        "conditions" => array(
                            "user_id" => $this->Session->read("Auth.User.id")
                        ),
                        "order" => array("UsersPage.title ASC", "UsersPage.created DESC")
                            )
            );
            $this->set("users_page", $ids);
            array_walk($ids, create_function('&$v,$k,$arr', 'array_push($arr[\'0\'],$v[\'UsersPage\'][\'page_id\']);'),
                    array(&$tmp));
            unset($ids);
            $this->Page->recursive = 0;
            $this->Page->Behaviors->attach("Containable");
            $this->Page->contain();
            $conds = array(
                "or" => array(
                    "Page.title LIKE" => '%' . $search . '%',
                    "Page.url_original LIKE" => '%' . $search . '%'
                ),
                "and" => array(
                    "Page.id" => $tmp
                )
            );
            $this->paginate = array(
                "conditions" => $conds,
                "limit" => 6,
                "contain" => array()
            );
            unset($tmp);
            $favorites = $this->paginate();
        }
        $this->set(compact('favorites'));
    }

    function delete_favorite($page_id=null)
    {
        $this->layout = "ajax";
        $this->Page->UsersPage->deleteAll(array(
            "user_id" => $this->Session->read("Auth.User.id"),
            "page_id" => $page_id
        ));
        $this->redirect("/favorites/index");
    }

    function add_favorite($id=null, $title=null)
    {
        $this->autoRender = false;
        Configure::write("debug", 0);
        $this->layout = "ajax";
        $ret = new stdClass;
        if (!$this->Session->check("Auth.User")) {
            $ret->message = "You cannot save favorites without being being logged in";
        } else {
            $this->Page->recursive = 0;
            $page = $this->Page->read(null, $id);
            if (!$this->Page->UsersPage->hasAny(array(
                        "user_id" => $this->Session->read("Auth.User.id"),
                        "page_id" => $id
                    )) && $this->Page->UsersPage->save(array(
                        "UsersPage" => array(
                            "user_id" => $this->Session->read("Auth.User.id"),
                            "page_id" => $id,
                            "title" => (!empty($title)) ? $title : $page['Page']['title']
                        )
                    ))) {
                $ret = true;
            } else {
                $ret->message = "This item is already on your favorites";
            }
        }
        echo json_encode($ret);
    }

    function create()
    {
        if (!empty($this->data)) {
            if ($this->Session->check("Auth.User")) {
                $this->data['Page']['user_id'] = $this->Session->read("Auth.User.id");
            }
            $this->data['Page']['master'] = $this->_getRandomWord();
            $this->Session->write("url_to_process", $this->data['Page']['url_original']);
            $password = (isset($this->data['Page']['password']) &&
                    !empty($this->data['Page']['password'])) ? $this->data['Page']['password']
                        : null;
            $this->redirect("/pages/init/" . $this->data['Page']['master'] . "/{$password}");
        }
    }

    function unlock($id=null)
    {
        $this->layout = "pad";
        if (!empty($this->data)) {
            $this->Page->id = $id;
            if ($this->data['Page']['password'] == $this->Page->field("password")) {
                $unlocked = ($this->Session->check("unlocked")) ? $this->Session->read("unlocked")
                            : array();
                if (!in_array($id, $unlocked)) {
                    $unlocked[] = $id;
                }
                $this->Session->write("unlocked", $unlocked);
                $this->redirect("/{$this->Page->field("url")}");
            } else {
                $this->Session->setFlash(__('The password is not valid.', true));
            }
        }
        $this->set("page_id", $id);
    }

    function isUnlocked($id=null)
    {
        if (!empty($id) && $this->Session->check("unlocked")) {
            $unlocked = $this->Session->read("unlocked");
            return in_array($id, $unlocked);
        }
        return false;
    }

    function init($name=null, $password=null)
    {
        $master = (!empty($name)) ? $name : $this->Session->read("Auth.User.username");
        $this->autoRender = false;
        if (!empty($name)) {
            if ($this->Page->hasAny(array("master" => $master))) {
                $this->redirect("/{$this->Session->read("Auth.User.username")}");
            }
        }
        if (!$this->Page->hasAny(
                        array(
                            "user_id" => $this->Session->read("Auth.User.id")
                        )
                ) || !empty($name)) {
            $this->Page->create();
            $this->Page->save(array(
                "Page" => array(
                    "user_id" => $this->Session->read("Auth.User.id"),
                    "master" => $master,
                    "url" => $master,
                    "title" => $master,
                    "password" => $password,
                    "public" => ((!empty($password)) ? false : true)
                )
                    ), false);
        }
        $this->redirect((!empty($name)) ? "/{$name}" : "/{$this->Session->read("Auth.User.username")}");
    }

    function saveOrder()
    {
        $this->autoRender = false;
        $this->layout = "ajax";
        Configure::write("debug", 0);
        if (empty($this->data)) {
            echo json_encode(false);
            return;
        }
        $col = $this->data['Page']['col'];
        if (!empty($_POST['page'])) {
            foreach ($_POST['page'] as $order => $id) {
                $this->Page->id = $id;
                $this->Page->save(array("col" => $col, "order" => $order));
            }
        }
        echo json_encode(true);
    }

    function scraper()
    {
        if ($this->Session->check("url_to_process")) {
            $this->Session->delete("url_to_process");
        }
        $this->layout = "ajax";
        Configure::write("debug", 0);
        App::import("Vendor", "simple_html_dom");
        $text = array();
        $img = array();
        $embed = null;
        $title = "Hurli";
        if (!empty($this->data)) {
            $url = (!empty($this->data['Page']['url_original'])) ? $this->data['Page']['url_original']
                        : null;
            $url_original = $url; // store same url so JS will SHA1 accordignly
            $url = (!preg_match("/^https?:\/\/.*/si", $url)) ? "http://" . $url
                        : $url;
            if ($url) {
                $this->url = preg_replace("/^(.*)\/?$/si", "$1", $url);
                $this->host = preg_replace("/^(https?:\/\/[^\/]+).*$/si", "$1", $url);
                $this->host = (!preg_match("/^https?:\/\/www\./si", $this->host))
                            ?
                        preg_replace("/^(https?:\/\/)(.*)$/si", "$1www.$2", $this->host)
                            : $this->host;
                $this->imagesFolder = WWW_ROOT . "img/pages/" . sha1($url_original) . "/";
                $isImage = (preg_match("/^.*\.(jpe?g|png|gif)$/si", $this->url))
                            ? true : false;
                if (!$isImage) {
                    try {
                        $headers = array(
                            'http' => array(
                                'method' => "GET",
                                'header' =>
                                "User-Agent: Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.3) Gecko/20100423 Ubuntu/10.04 (lucid) Firefox/3.6.3\r\n" .
                                "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n" .
                                "Accept-language: en\r\n" .
                                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n"
                            )
                        );
                        $ctx = stream_context_create($headers);
                        $html = file_get_html($url, false, $ctx);
                        // get title
                        $tt = $html->find('title', 0);
                        if (is_object($tt)) {
                            $encoding = mb_detect_encoding($tt->innertext, "UTF-8,ISO-8859-1,WINDOWS-1252");
                            $tt->innertext = (!stristr($encoding, "UTF-8"))
                                        ? utf8_encode($tt->innertext) : $tt->innertext;
                            $title = trim($tt->innertext);
                            $title = (!empty($title)) ? $title : $this->url;
                        } else {
                            $title = $this->url;
                        }
                        // get images
                        $this->_getImg($html, $img);
                        // get text
                        $this->_getText($html, $text);
                        $videoSite = false;
                        // if youtube
                        if (preg_match("/youtube.com/si", $this->host)) {
                            $this->_youTube($embed);
                            $videoSite = true;
                        }
                        // if vimeo
                        if (preg_match("/vimeo.com/si", $this->host)) {
                            $this->_vimeo($embed);
                            $videoSite = true;
                        }
                        // if dailymotion
                        if (preg_match("/dailymotion.com/si", $this->host)) {
                            $this->_dailyMotion($embed);
                            $videoSite = true;
                        }
                        // try to find a video
                        if (!$videoSite) {
                            $this->_getVideoObject($html, $embed);
                        }
                        $html->clear();
                        unset($html);
                    } catch (Exception $e) {

                    }
                } else {
                    $name = null;
                    $collected = array($this->url);
                    $this->_saveImagesOnDisk($collected, $img);
                    array_push($img, $name);
                }
            }
        }
        $this->set("title", $title);
        $this->set("text", $text);
        $this->set("url_original", $url_original);
        $this->set("hash", sha1($url_original));
        $categories = $this->Page->Category->find("list");
        $this->set(compact('categories'));
        if (empty($embed)) {
            $img = array_filter($img);
            $this->set("img", $img);
        }
        $this->set("embed", $embed);
    }

    function _getVideoObject(&$html=null, &$embed=null)
    {
        foreach ($html->find('object,embed') as $e) {
            $obj = trim(preg_replace("/[\s\n]{1,}/s", " ", $e->outertext));
            if (preg_match("/\.flv|\.swf|src\=/si", $obj)) {
                $w = (int) preg_replace("/.*(width|w)\=?[\"\']?(\d+)[\"\']?.*/si", "$2", $obj);
                $h = (int) preg_replace("/.*(height|h)\=?[\"\']?(\d+)[\"\']?.*/si", "$2", $obj);
                if ($w > 0 && $h > 0) {
                    $obj = preg_replace("/width\=[\"\']?\d+[\"\']?/si", "width=\"" . EMBED_VIDEO_W . "\"", $obj);
                    $obj = preg_replace("/height\=[\"\']?\d+[\"\']?/si", "height=\"" . EMBED_VIDEO_H . "\"", $obj);
                    $obj = preg_replace("/w\=[\"\']?\d+[\"\']?/si", "w=\"" . EMBED_VIDEO_H . "\"", $obj);
                    $obj = preg_replace("/w\=\d+/si", "w=" . EMBED_VIDEO_W, $obj);
                    $obj = preg_replace("/h\=\d+/si", "h=" . EMBED_VIDEO_H, $obj);
                    $embed = $obj;
                    break;
                }
            }
        }
    }

    function _vimeo(&$embed=null)
    {
        $vimeoId = preg_replace("/.*\/(\d+).*/si", "$1", $this->url);
        $embed = '
<div style="text-align: center; margin: auto"><object type="application/x-shockwave-flash" style="width:' . EMBED_VIDEO_W . 'px; height:' . EMBED_VIDEO_H . 'px;" data="http://vimeo.com/moogaloop.swf?clip_id=' . $vimeoId . '&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=ff9933&amp;fullscreen=1">
<param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=' . $vimeoId . '&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=ff9933&amp;fullscreen=1" />
    <param name="wmode" value="opaque" />
</object></div>  
        ';
    }

    function _youTube(&$embed=null)
    {
        $youtubeId = preg_replace("/.*v\=([^\/\?\&]+).*|.*\?([^\/\?\&]+).*/si", "$1", $this->url);
        $embed = '
<div style="text-align: center; margin: auto"><object type="application/x-shockwave-flash" style="width:' . EMBED_VIDEO_W . 'px; height:' . EMBED_VIDEO_H . 'px;" data="http://www.youtube.com/v/' . $youtubeId . '?rel=0&amp;showsearch=0&amp;fs=1">
<param name="movie" value="http://www.youtube.com/v/' . $youtubeId . '?rel=0&amp;showsearch=0&amp;fs=1" /><param name="allowFullScreen" value="true" />
    <param name="wmode" value="opaque" />
</object></div>
        ';
    }

    function _dailyMotion(&$embed=null)
    {
        $dmId = preg_replace("/^.*\/([^\/]+)$|^.*\/([^\/\_]+)\_.*$/si", "$1", $this->url);
        $embed = '<div style="text-align: center; margin: auto">
<object type="application/x-shockwave-flash" width="' . EMBED_VIDEO_W . '" height="' . EMBED_VIDEO_H . '" data="http://www.dailymotion.com/swf/' . $dmId . '"><param name="movie" value="http://www.dailymotion.com/swf/' . $dmId . '" />
<param name="wmode" value="opaque" />
</object>
        </div>';
    }

    function _getImg(&$html=null, &$img=null)
    {
        if (isset($html)) {
            $c = 1;
            $collected = array();
            $info = array();
            foreach ($html->find('img') as $e) {
                $src = $e->src;
                if (preg_match("/jpe?g|gif|png/si", $src)) {
                    $url = (!preg_match("/^https?:\/\/.*$/si", $src)) ? $this->host . "/" .
                            preg_replace("/^\/(.*)$/", "$1", $src) : $src;
                    array_push($collected, $url);
                    unset($info);
                }
                if ($c++ >= SCRAPER_MAX_IMG_ITERATE) {
                    break;
                }
            }
            $this->_saveImagesOnDisk($collected, $img);
            unset($collected);
        }
    }

    function _saveImagesOnDisk(&$collected=null, &$img=null)
    {
        if (!empty($collected)) {
            $tmp = array();
            foreach ($collected as $url) {
                $data = @file_get_contents($url, FILE_BINARY);
                if (!empty($data)) {
                    $ext = preg_replace("/^.*\.([a-z]{3,4})$/si", "$1", $url);
                    if (!is_dir(WWW_ROOT . "img/pages")) {
                        mkdir(WWW_ROOT . "img/pages", 0777);
                    }
                    if (!is_dir($this->imagesFolder)) {
                        mkdir($this->imagesFolder, 0777);
                    }
                    $hash = sha1(ceil(rand(10, 100000) * microtime()));
                    $name = trim(strtolower($hash . "." . $ext));
                    $newName = $this->imagesFolder . $name;
                    $f = new File($newName, true, 0755);
                    $f->write($data);
                    $f->create();
                    unset($data);
                    unset($f);
                    $info = @getimagesize($newName);
                    $info['url'] = $name;
                    array_push($tmp, $info);
                }
            }
            $this->_getMostRelevantImage($tmp, $img);
        }
    }

    function _resizeImage(&$name=null)
    {
        if (!empty($name)) {
            App::import("Component", "PImage");
            $pimage = new PImageComponent();
            $pimage->resizeImage("resizeCrop", $name, $this->imagesFolder, false, EMBED_VIDEO_W, EMBED_VIDEO_H, 100);
        }
    }

    function _getMostRelevantImage(&$collected, &$img)
    {
        if (!empty($collected)) {
            uasort($collected, "sort_imageresize");
            foreach ($collected as $info) {
                if (@filesize($this->imagesFolder . $info['url']) < 1024) {
                    @unlink($this->imagesFolder . $info['url']);
                } else {
                    $this->_resizeImage($info['url']);
                    array_push($img, $info['url']);
                }
            }
        }
    }

    function _getText(&$html=null, &$text=null)
    {
        if (isset($html)) {
            foreach ($html->find('body') as $ee) {
                foreach ($ee->find("text") as $e) {
                    $encoding = mb_detect_encoding($e->plaintext, "UTF-8,ISO-8859-1,WINDOWS-1252");
                    $e->plaintext = (!stristr($encoding, "UTF-8")) ? utf8_encode($e->plaintext)
                                : $e->plaintext;
                    $t = preg_replace("/[\s\n]{1,}/si", " ", trim($e->plaintext));
                    if (strlen($t) > 50 && !preg_match("/[^\s]{30,}/si", $t)) {
                        if (!preg_match("/\#[^\{]+\{|var [\w\s]*\=|new \w\s*\([^\)]+\)|\w\s*\([^\)]+\)\s*;|function\s*\([^\)]*\)\s*\{[^\}]*\}|\.\w+\s*\{[^\}]*\}|\w+\s*\=\s*[^\;]+\;/si", $t)) {
                            $t = preg_replace("<\!\-\-.*\-\->", "", $t);
                            $text[] = $t;
                        }
                    }
                }
            }
        }
    }

    function index($master=0, $keywords=null)
    {
        $this->layout = "pad";
        $this->Page->Behaviors->attach("Containable");
        $this->Page->contain();
        if (!empty($master) && $master != 0) {
            $this->data = $this->Page->find("first", array("conditions" =>
                        array(
                            "master" => $master,
                            "title" => $master
                            ))
            );
            $this->Session->write("master", $master);
            if (!empty($this->data['Page']['password']) && !$this->isUnlocked($this->data['Page']['id'])) {
                $this->redirect(array("action" => "unlock", $this->data['Page']['id']));
            }
        } /* else if ($master != 0) {
          $this->Session->delete("master");
          } */
        $this->paginate = array(
            "contain" => array("User", "User.id", "User.admin", "User.username", "Comment"),
            "order" => array(
                "Page.col ASC",
                "Page.order ASC"
            )
        );
        $this->paginate['conditions'] = (!empty($master)) ?
                array("master" => $master) :
                ((!empty($keywords)) ?
                        array("or" => array("Page.keywords LIKE" => ((!empty($keywords))
                                    ?
                                '%' . $keywords . '%' :
                                null), "Page.url_original LIKE" => '%' . $keywords . '%',
                        "Page.title LIKE" => '%' . $keywords . '%',
                        "Page.url LIKE" => '%' . $keywords . '%',
                        "Page.content LIKE" => '%' . $keywords . '%'),
                    "and" => array(
                        "Page.master" => $this->Session->read("master")
                        )) :
                        array("Page.user_id" => $this->Session->read("Auth.User.id")));
        $pages = $this->paginate();
        if (!empty($pages)) {
            if (empty($keywords)) {
                $this->set("title_for_layout", "pages of " . $pages['0']['User']['username']);
            } else {
                $this->set("title_for_layout", "searching for " . $keywords);
            }
        }
        $this->set('pages', $pages);
        $this->set('master', $master);
    }

    function view($id = null)
    {
        if ($id) {
            $this->set('page', $this->Page->read(null, $id));
        }
    }

    function cleanFiles($id=null)
    {
        if (!empty($id)) {
            $this->autoRender = false;
            $this->Page->id = $id;
            $this->Page->recursive = 0;
            if (preg_match("/^\/img\/.*$/si", $this->Page->field("media"))) {
                $url_original = $this->Page->field("url_original");
                $tmp = $this->Page->find("all", array("fields" => array("media"),
                            "conditions" => array("SHA1(url_original)" => sha1($url_original))
                                )
                );
                $history = array();
                array_walk($tmp, create_function('&$v,$k,&$arr', 'array_push($arr[\'0\'],$v[\'Page\'][\'media\']);'),
                        array(&$history));
                unset($tmp);
                $pagesFolder = WWW_ROOT . "img/pages/";
                $folder = $pagesFolder . sha1($url_original);
                if (is_dir($folder)) {
                    $d = opendir($folder);
                    if ($d) {
                        while ($r = readdir($d)) {
                            $f = "/img/pages/" . sha1($url_original) . "/" . $r;
                            if (!in_array($f, $history)) {
                                @unlink($folder . "/" . $r);
                            }
                        }
                    }
                }
            }
        }
    }

    function load()
    {
        $url = (isset($this->params['url']) && isset($this->params['url']['url']))
                    ? $this->params['url']['url'] : null;
        if (!$url) {
            $this->cakeError("notFound");
        }
        $isMaster = $this->Page->hasAny(array("master" => $url));
        $page = $this->Page->find("first", array("conditions" =>
                    array(
                        "or" => array(
                            array("url" => $url),
                            array("master" => $url)
                        )
                        ))
        );
        // check if master page has password
        $master = $this->Page->find("first", array("conditions" => array("url" => $page['Page']['master'])));
        $page['Page']['password'] = $master['Page']['password'];
        if (!empty($page['Page']['password']) && !$this->isUnlocked($master['Page']['id'])) {
            $this->redirect(array("action" => "unlock", $master['Page']['id']));
        }
        $this->Session->write("master", $page['Page']['master']);
        if (!$page || empty($page)) {
            $this->cakeError("notFound");
        }
        $this->layout = "pad";
        $this->Session->write("current_page", $page['Page']['id']);
        $this->set(compact('page'));
        $this->set("is_master", $isMaster);
        $this->set("url", $url);
        $this->Page->recursive = 0;
        /**
         * SET META TAGS
         */
        $k = $this->Page->find("first", array(
                    "fields" => array("GROUP_CONCAT(DISTINCT Page.keywords SEPARATOR ', ') as keywords"),
                    "conditions" => array("Page.master" => $page['Page']['master'])
                ));
        $keywords = ((isset($k['0']['keywords'])) ? $k['0']['keywords'] : null);
        $this->set('meta_description', $keywords);
        $this->set('meta_keywords', $keywords);
        $this->set("title_for_layout", $page['Page']['title'] . " | " . rtrim(substr($keywords, 0, 55), ","));
    }

    function add()
    {
        $this->autoRender = false;
        Configure::write("debug", 0);
        if (!empty($this->data)) {
            if (empty($this->data['Page']['password'])) {
                unset($this->data['Page']['password']);
            }
            if (!isset($this->data['Page']['password'])) {
                unset($this->data['Page']['password_confirm']);
            }
            $this->data['Page']['category_id'] = (!empty($this->data['Page']['category_id']))
                        ? $this->data['Page']['category_id'] : 1;
            $this->data['Page']['master'] = $this->Session->read("master");
            $this->data['Page']['user_id'] = $this->Session->read("Auth.User.id");
            $this->data['Page']['url'] = $this->_getAvailableUrl();
            $this->data['Page']['keywords'] = $this->_getKeywords();
            $this->Page->create();
            if ($this->Page->save($this->data)) {
                $this->Page->id = $this->Page->getLastInsertID();
                $ret = new stdClass;
                $ret->view = $this->requestAction("/page-view/{$this->Page->id}", array("return"));
                $this->cleanFiles($this->Page->id);
                echo json_encode($ret);
            } else {
                $ret = new stdClass;
                $ret->message = implode('<br />', $this->Page->invalidFields());
                echo json_encode($ret);
            }
        }
    }

    function _getKeywords()
    {
        $ret = null;
        $this->common_words = explode(",", "'tis,'twas,a,able,about,across,after,ain't,all,almost,also,am,among,an,and,any,are,aren't,as,at,be,because,been,but,by,can,can't,cannot,could,could've,couldn't,dear,did,didn't,do,does,doesn't,don't,either,else,ever,every,for,from,get,got,had,has,hasn't,have,he,he'd,he'll,he's,her,hers,him,his,how,how'd,how'll,how's,however,i,i'd,i'll,i'm,i've,if,in,into,is,isn't,it,it's,its,just,least,let,like,likely,may,me,might,might've,mightn't,most,must,must've,mustn't,my,neither,no,nor,not,of,off,often,on,only,or,other,our,own,rather,said,say,says,shan't,she,she'd,she'll,she's,should,should've,shouldn't,since,so,some,than,that,that'll,that's,the,their,them,then,there,there's,these,they,they'd,they'll,they're,they've,this,tis,to,too,twas,us,wants,was,wasn't,we,we'd,we'll,we're,were,weren't,what,what'd,what's,when,when,when'd,when'll,when's,where,where'd,where'll,where's,which,while,who,who'd,who'll,who's,whom,why,why'd,why'll,why's,will,with,won't,would,would've,wouldn't,yet,you,you'd,you'll,you're,you've,your");
        if (!empty($this->data) && !empty($this->data['Page']['content'])) {
            $tmp = strtolower(trim(strip_tags($this->data['Page']['content'] . " " . $this->data['Page']['title'])));
            $tmp = preg_replace("/\b(" . implode("|", $this->common_words) . ")\b/si", "", $tmp);
            $arr = preg_split("/[\s\.\-\_\;\:\,\"\'\&\|\/]+/", $tmp, null, PREG_SPLIT_NO_EMPTY);
            $ret = implode(",", $arr);
        }
        return $ret;
    }

    function _getAvailableUrl()
    {
        if (!empty($this->data)) {
            $url = strtolower(preg_replace("/[^a-z0-9]{1,}/si", "-",
                                    iconv("UTF-8", "ASCII//TRANSLIT", trim($this->data['Page']['title']))));
            if ($this->Page->hasAny(array("Page.url" => $url))) {
                $url .= "-" . $this->_getRandomWord();
            }
        }
        return $url;
    }

    function manager_index()
    {
        $this->Page->recursive = 0;
        $this->set('pages', $this->paginate());
    }

    function manager_view($id = null)
    {
        if (!$id) {
            $this->Session->setFlash(sprintf(__('Invalid %s', true), 'page'));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('page', $this->Page->read(null, $id));
    }

    function manager_add()
    {
        $this->cacheAction = false;
        if (!empty($this->data)) {
            if (empty($this->data['Page']['password'])) {
                unset($this->data['Page']['password']);
            }
            if (!isset($this->data['Page']['password'])) {
                unset($this->data['Page']['password_confirm']);
            }
            $this->data['Page']['category_id'] = (!empty($this->data['Page']['category_id']))
                        ? $this->data['Page']['category_id'] : 1;
            $this->Page->create();
            if ($this->Page->save($this->data)) {
                $this->Session->setFlash(sprintf(__('The %s has been saved', true), 'page'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'page'));
            }
        }
        $categories = $this->Page->Category->find("list");
        $this->set(compact('categories'));
    }

    function manager_edit($id = null)
    {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(sprintf(__('Invalid %s', true), 'page'));
            $this->redirect(array('action' => 'index'));
        }
        if (empty($this->data['Page']['password'])) {
            unset($this->data['Page']['password']);
        }
        if (!empty($this->data)) {
            $this->Page->id = $this->data['Page']['id'];
            if (isset($this->data['Page']['url']) && $this->Page->field("url") == $this->data['Page']['url']) {
                unset($this->data['Page']['url']);
            }
            if (!isset($this->data['Page']['password'])) {
                unset($this->data['Page']['password_confirm']);
            }
            $this->data['Page']['category_id'] = (!empty($this->data['Page']['category_id']))
                        ? $this->data['Page']['category_id'] : 1;
            $oldUrl = $this->Page->field("url");
            if ($this->Page->save($this->data)) {
                if (isset($this->data['Page']['url'])) {
                    $this->Page->recursive = -1;
                    $this->Page->updateAll(array("Page.master" => $this->data['Page']['url']), array("Page.master" => $oldUrl));
                }
                $this->Session->setFlash(sprintf(__('The %s has been saved', true), 'page'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'page'));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Page->read(null, $id);
        }
        $categories = $this->Page->Category->find("list");
        $this->set(compact('categories'));
    }

    function manager_delete($id = null)
    {
        if (!$id) {
            $this->Session->setFlash(sprintf(__('Invalid id for %s', true), 'page'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Page->id = $id;
        $this->Page->recursive = 0;
        $media = $this->Page->field("media");
        $media = (preg_match("/^\/img\/.*$/si", $media)) ?
                ltrim($media, "/") : null;
        if (!empty($media)) {
            @unlink(WWW_ROOT . $media);
        }

        if ($this->Page->delete($id)) {
            $this->Session->setFlash(sprintf(__('%s deleted', true), 'Page'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(sprintf(__('%s was not deleted', true), 'Page'));
        $this->redirect(array('action' => 'index'));
    }

    /**
     * Displays a view
     *
     * @param mixed What page to display
     * @access public
     */
    function display()
    {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));
        $this->render(implode('/', $path));
    }

    function _loadStaticPage($page=null)
    {
        if (!empty($page)) {
            $out = null;
            $static_page = $this->StaticPage->find("first", array(
                        "conditions" => array(
                            "StaticPage.title" => $page
                        )
                    ));
            $this->set(compact('static_page'));
        }
    }

    function manager_static()
    {
        $this->StaticPage = & ClassRegistry::init("StaticPage", "Model");
        $this->layout = "default";
        if (!empty($this->data)) {
            $this->StaticPage->id = $this->data['StaticPage']['id'];
            $this->StaticPage->save($this->data);
        }
        $static_pages = $this->StaticPage->find("all");
        $this->set(compact('static_pages'));
    }

}

?>