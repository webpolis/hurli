<?php

class UsersController extends AppController
{

    var $helpers = array('Html', 'Javascript', 'Text', 'Session');
    var $components = array('RequestHandler', 'Session', 'Captcha');
    var $name = 'Users';

    function beforeFilter()
    {
        $this->Auth->allow(array("add", "login", "logout", "forgot", "captcha_image"));
        $this->components[] = "Captcha";
        $this->Auth->autoRedirect = false;
        parent::beforeFilter();
    }

    function captcha_image()
    {
        $this->Captcha->image();
    }

    function forgot()
    {
        $this->User->recursive = 0;
        if (!empty($this->data)) {
            $user = $this->User->find("first", array(
                        "conditions" => array(
                            "or" => array(
                                "email" => $this->data['User']['email'],
                                "username" => $this->data['User']['username']
                            )
                        )
                    ));
            if (!empty($user)) {
                $password = $this->_getRandomWord();
                $this->User->id = $user['User']['id'];
                App::import("Component", "Email");
                $email = & new EmailComponent();
                $email->initialize($this);
                $email->from = "hurli <noreply@hurli.com>";
                $email->to = "{$user['User']['username']} <{$user['User']['email']}>";
                $email->subject = "New password requested";
                $email->sendAs = "both";
                $email->template = "forgot";
                $this->set("password", $password);
                $this->set(compact('user'));
                $email->send();
                $this->Session->setFlash("Your new password has been sent.");
                $this->User->saveField("password", $this->Auth->password($password));
                $this->redirect($this->Session->read("last_page"));
            }
            else {
                $this->Session->setFlash("We haven't found any user that matches the provided information.");
            }
        }
        else {
            $this->Session->write("last_page", $this->referer());
        }
    }

    function login()
    {
        $user = $this->Auth->user();
        if ($user) {
            $this->User->Behaviors->attach("Containable");
            $this->User->contain();
            $this->User->id = $user['User']['id'];
            $data = $this->User->read();
            if ((bool) $data['User']['banned']) {
                $this->Session->setFlash("You were banned by the administrator");
                $this->redirect("/logout");
            }
            else {
                $this->Session->write("Auth", $data);
                if (!$this->Session->check("master")) {
                    $this->redirect("/pages/init/{$data['User']['username']}");
                }
                else {
                    $this->redirect("/{$this->Session->read("master")}");
                }
            }
        }
    }

    function logout()
    {
        $this->Session->destroy();
        $this->redirect($this->Auth->logout());
    }

    function index()
    {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    function view($id = null)
    {
        if (!$id) {
            $this->Session->setFlash(sprintf(__('Invalid %s', true), 'user'));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('user', $this->User->read(null, $id));
    }

    function add()
    {
        if (!empty($this->data)) {
            $this->User->create();
            if (!isset($this->data['User']['password']) || empty($this->data['User']['password'])) {
                unset($this->data['User']['password_confirm']);
                unset($this->data['User']['password']);
            }
            else {
                //$this->data['User']['password'] = $this->Auth->password($this->data['User']['password']);
                $this->data['User']['password_confirm'] = $this->Auth->password($this->data['User']['password_confirm']);
            }
            $this->data['User']['picture'] = "undefined.jpg";
            if ($this->User->save($this->data)) {
                $this->Session->setFlash(sprintf(__('You have been succesfully registered and logged.', true), 'user'));
                $this->Auth->login($this->data);
                if (!$this->Session->check("master")) {
                    $this->redirect("/");
                }
                else {
                    $this->redirect("/{$this->Session->read("master")}");
                }
            }
            else {
                $this->Session->setFlash(sprintf(__('The registration cannot be completed. Please, try again.', true), 'user'));
            }
        }
        $pages = $this->User->Page->find('list');
        $this->set(compact('pages'));
    }

    function edit()
    {
        $this->User->id = $this->Session->read("Auth.User.id");
        if (!empty($this->data)) {
            $this->_savePicture();
            if (!isset($this->data['User']['password']) || empty($this->data['User']['password'])) {
                unset($this->data['User']['password_confirm']);
                unset($this->data['User']['password']);
            }
            else {
                $this->data['User']['password'] = $this->Auth->password($this->data['User']['password']);
                $this->data['User']['password_confirm'] = $this->Auth->password($this->data['User']['password_confirm']);
            }
            if ($this->User->save($this->data)) {
                $this->Session->setFlash(sprintf(__('Your information has been updated', true), 'user'));
            }
            else {
                $this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'user'));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->User->read();
        }
    }

    function _savePicture()
    {
        if (!empty($this->data['User']['picture']) && file_exists($this->data['User']['picture']['tmp_name'])) {
            $tmp = new File($this->data['User']['picture']['tmp_name']);
            $data = & $tmp->read();
            unset($tmp);
            $folder = $folder = WWW_ROOT . "/img/users/";
            if (!is_dir($folder)) {
                @mkdir($folder, 0777);
            }
            $newname = sha1(microtime()) . ".jpg";
            $this->data['User']['picture'] = $newname;
            // save pic
            $dest = $folder . $newname;
            $new = new File($dest, true);
            $new->write($data);
            unset($new);
            // generate new pic
            App::import("Component", "PImage");
            $pimage = new PImageComponent();
            $pimage->resizeImage("resizeCrop", $newname, $folder, false, PROFILE_PIC_THUMB_W, PROFILE_PIC_THUMB_H, 100);
        }
    }

    function manager_index()
    {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    function manager_view($id = null)
    {
        if (!$id) {
            $this->Session->setFlash(sprintf(__('Invalid %s', true), 'user'));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('user', $this->User->read(null, $id));
    }

    function manager_add()
    {
        if (!empty($this->data)) {
            $this->User->create();
            if (!isset($this->data['User']['password']) || empty($this->data['User']['password'])) {
                unset($this->data['User']['password_confirm']);
                unset($this->data['User']['password']);
            }
            else {
                $this->data['User']['password_confirm'] = $this->Auth->password($this->data['User']['password_confirm']);
            }
            if ($this->User->save($this->data)) {
                $this->Session->setFlash(sprintf(__('The %s has been saved', true), 'user'));
                $this->redirect(array('action' => 'index'));
            }
            else {
                $this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'user'));
            }
        }
    }

    function manager_edit($id = null)
    {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(sprintf(__('Invalid %s', true), 'user'));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if (!isset($this->data['User']['password']) || empty($this->data['User']['password'])) {
                unset($this->data['User']['password_confirm']);
                unset($this->data['User']['password']);
            }
            else {
                $this->data['User']['password'] = $this->Auth->password($this->data['User']['password']);
                $this->data['User']['password_confirm'] = $this->Auth->password($this->data['User']['password_confirm']);
            }
            if ($this->User->save($this->data)) {
                $this->Session->setFlash(sprintf(__('The %s has been saved', true), 'user'));
                $this->redirect(array('action' => 'index'));
            }
            else {
                $this->Session->setFlash(sprintf(__('The %s could not be saved. Please, try again.', true), 'user'));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->User->read(null, $id);
        }
    }

    function manager_delete($id = null)
    {
        if (!$id) {
            $this->Session->setFlash(sprintf(__('Invalid id for %s', true), 'user'));
            $this->redirect(array('action' => 'index'));
        }
        $this->User->id = $id;
        $admin = (bool) $this->User->field("admin");
        if (!$admin) {
            if ($this->User->delete($id)) {
                $this->Session->setFlash(sprintf(__('%s deleted', true), 'User'));
                $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(sprintf(__('%s was not deleted', true), 'User'));
            $this->redirect(array('action' => 'index'));
        }
        else {
            $this->Session->setFlash("You cannot delete an administrator");
            $this->redirect(array('action' => 'index'));
        }
    }

}

?>